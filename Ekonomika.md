# Ekonomika
*Matej Vrba*

**Kdyz dostanu z testu 4 a hure tak pristi hodinu budu zkousen!**  
 
## Hodina 1.
*Tuesday, 10. September 2019*

**Odevzdavani sesitu ke kontrole:**  
 *Tuesday, 17. September 2019* 

Ekonomika x Ekonomie
Ekonomie -> OIKONOMOS
Ekonomie je teoreticky odborny obor, tak se nazyva i predmet na vysoke skole, ktery se zabiva hospodarskymi problemy cloveka, jak jednotlivce tak i spolecnosti.

**Cim se ekonomika lisi od ekonomie?**  
Ekonomika se zajima o prakticke hospodareni.
Hospodarka situace *neceho*
Vetsinou se na neco zameruje.

**Hospodareni**  
Je to nakladani s nejakymi prostredky.

**Cil**  
Jednotlivec, domacnost -> Uspokojeni potreb
Podniky -> Zisk
Stat -> Splneni zakladnich funkci statu

### Lidske Potreby  
Nerovnovaha organismu -> Uspokojeni

## Hodina 2.  
*September 17, 2019*  

## Lidske Potreby  
Druhy  
- Primarni (Prvotni) x Sekundarni (Druhotne)

**Primarni:**  
*Zivotne dulezite*  
* Jidlo a Piti  
* Dychani  
* Spanek  
* Komunikace se spolecnosti  
* Hygiena  


**Sekundarni:**  
*Nejde o zivot*
Kvalitnejsi uspokojovani primarnich potreb
* Dat si biftek
* Ochrana klimatu
* Ucit se spanelstinu
* Volnocasove aktivity

## Hodina 3.
*September 24, 2019*


Je dulezite doplnovat psychicke a fyzicke potreby
Napriklad u deti je dulezite objimani a davani jim najevo lasky
  
**Individualni:**  
Skola chce ucit pouze chytre lidi, lidi chteji pouze dobre ucitele
Jizda autem do skoly

**Kolektivni:**  
Stat chce vzdelani pro vsechny
Autobus pro studenty

**Domaci Ukol:**

|n.|Potreba|Prim. x Sek.|Fyz. x Psych.|Indi. x Kolek.|
|--|--|--|--|--|
|1.|Dýchat|Primární|Fyzická|Individualní|
|2.|Kálet|Primární|Fyzická|Individualní|
|3.|Získávat živyny|Prímání|Fyzická|Individualní|
|4.|Kouření|Sekundární|Psychická|Individualní|
|5.|Plnit dopravi predpisy|Sekundární|Psychická|Kolektivní|
|6.|Hygiena|Primární|Fyzická|Indivudualní|
|7.|Komunikace|Primární|Psychická|Individualní|
|8.|Zavírat toaletu|Sekundární|Psychická|Individualní|
|9.|Poslouchat hudbu|Sekundární|Psychická|Individualní|
|10.|Vzdelaní|Primarní|Psychická|Kolektivní|

### Statky a Sluzby

#### Statky
1. **Potreba / Pouziti**
zpusob jakych to pouzijeme 
zpusob uspokojeni  
2. **Hmotne / Nehmotne**  
Podstata  
Napriklad Pythagorova veta, Patenty, Myslenka
3. **Volne / Ekonomicke**  
Podle dostupnosti
Volny statek je scela neomezeny > solarni panely
Ekonomicky > Uhli


## Hodina 4.  
*October 1, 2019*  

### Sluzby  
Cinnosti jinych osob ktera uspokojuje nase potreby  
**osobni / vecne**  
- osobni delaji lide
- vecne se tykaji predmetu ktere pouzivame (vymalovat byt)

**prubeh / vysledek**  
- jde o to co probiha behem sluzby
- jde o vysledek samotne sluzby


## Ukol
|Statek|Spotreba / Pouziti | Hmotne / Nehmotne |Volne / Ekonomicke|
|---|---|---|---|
|Napad|Pouziti|Nehmotny|Volne|
|Uklid|Pouziti|Nehmotny|Volne|
|Spagety|Spotreba|Hmotne|Ekonomicky|
|Zelezo|Spotreba|Hmotne|Ekonomicky|
|Pratelstvi|Pouziti|Nehmotne|Volne|

|Sluzby |Osobni / Vecne | Prubeh / Vysledek |
|---|---|---|
|Vareni|Osobni|Vysledek|
|Zpev|Osobni|Prubeh|
|Postaveni domu|Vecne|Vysledek|
|Najemny vrah|Osobni|Vysledek|
|Zahradnik|Vecne|Vysledek|

## Hodina 5.

### Zivotni Uroven

- Souhrnny ukazatel
- Kvalita + Kvantita uspokojovani
- Znaky Prijmu >
  - Prijmi ze zavisle cinnosti
  - osoba samostne vydelecne cinna (podnikani)
  - Socialni davka
  - Samozasobitlekstvi
  - 

## Hodina 6.
Film

## Hodina 7.
Prace:  
- Cilevedoma lidska cinnost
- s Odmenou vetsinou

Pracovni sila:
- Dusevni + Fyzicke schopnosti pouzivane pri praci
- Casto jsou to pracovnici (zjednodusene)  

## Hodina 8.

Pracovni proces  
- Pracovni sila
- Prostredky Jednorazove Spotreby 
  - pretvari se
  - electricka energie
- Prostredky postupne spotreby
  - nepretvari se
  - nemesi svou fyzickou podobu
  - opotrebovava se
    - Fyzicke
      - Pouzivanim > Autoskola auto
      - Projev > Spatne radi
    - Moralni
      - Duvod > Technicky pokrok
      - Projev > Technicky zaostale

DU tabulku 
Prac. Sila , Prostredky jednorazove Spotreby, Pros. postu. spot.  
Svadlena > Latka, Sici stroj  

|Pracovni Sila|Prostredky jednorazove spotreby| Prostredky postupne spotreby|
|---|---|---|
|Programator|Elektrina|Pocitac|
|Pekar|Testo (Mouka,Vajicka, Sul, Mleko)|Pec|
|Hornik|Elektrina / Hrot zbijecky|Zbijecka|
|Tesar|Mramor|Kladivko|
|Tatecnik|Elektrinu|Boty|
|Popelar|Vonavku|Oblek|
|Policista|Papir na pokuty| Propisku|
|Ucitel|Papiry|Cervene propisky|
|Malir|Papir|Barva|
|Kadernik|Vlasy|Nuzky|


## Hodina 9.

### Delba prace
Rozdeleni:  

* obory
* odvetvi
* procesy

Vyvoj:  

* Muz x Zena
* Sber
* lov
* Remeslo
* Pestitelstvi
* Chov
* Obchod / smena
* Nematerialni cinnosti, leceni, vedeni
* umeni, vzdelani / vychova
* obrana
* Soucasnost
* specializace > uzke zamereni  
  * Vyhody:
    * Zamereni
    * Vysoka kvalita
  * Nevyhody:
    * zavislost na okoli
    * vyvojove zmeny

* Kooperace

Stupen rozvoje delby prace, vypovida o vyspelist spolecnosti (ekonomiky a technicky)  
* umuzousti indiani
* euroatlanticka spoleecnost
* ropne mocnosti

## Hodina 10.

### Hospodarsky proces celospolecnost  

Vyroba > Rozdelovani > Smena > Spotreba > zpet vyroba  

Vytvareni statku s sluzeb pro cizi spotrebu!  

Rozdelovani > Distribuce --> dodani zakaznikovi  

Smena > Nakup a prodej statku a sluzek (Ne vzdy to platime mi (casto napriklad stat))  

Spotreba > 

## Hodina 11.

~~Nebyl~~

## Hodina 12.

Vztahy v hospodarskem procesu
- Ciste statni podniky jsou i podnikem
- Stat - Obyvatelstvo
  - Obyvatel odvadi statu dane
  - Pracovni sila obyvatele statu
  - Verejne statky a sluzby stat obyvatelum
- Stat - Podniky
  - Dane statu
  - Sluzby pro stat
  - stat je pro podniky zakaznik
  - stat nabizi verejne statky a sluzby
- Podniky - obyvatel
  - Sluzby pro obyvatele
  - Obyvatele plati za sluzby
  - Pracovni sila pro firmy
  - Pracovni mista pro obyvatele  

POMOC  

Reprodukce:  

- Opakovani Hospodarskeho procesu
  - Prosta reprodukce = Stejny rozsah
  - Rozsirena = postupne roste
  - Zuzena = klesa, muze az vymrit
  - 

## Hodina 14.

**Pisemne opakovani**  

##  Hodina 15.

ČR > Tržní hospodářství od 1989 
Centrální hospodárství - od 1948  
Před 1948 - Tržní hospodářství > od 19. stol.  
Před rokem 1989 jsme nemohl soukromně vlastnit podnik.  

## Hodina 16.

|Trzni Hospodarstvi|Centralne Planovane Hospodarstvi|
|---|---|
|Soukrome vlastnictvi|Stat vlastni vse|
|Pravni samostatnost - obor / zamestnance / spolupracovniky|stat urcije co / komu / kdo |
|Ekonomicka samostnatnost - Cena / Rozdeleni zisku| Ekonomicka nesamostnatnost > 80% dan statu |
|Volna smenitelnost meny|Neexistuje volna smenitelnost meny|

## Hodina 17.

**Pisemne opakovani**  

Trh > stret nabidky a poptavky  

Nabidka:  
- Čas
- Misto
- Cena
  
Poptavka:  
- Čas
- Misto
- Cena

## Hodina 18.  

Poptavkova pruznost > jak moc jsme ochotní priplatit za vec  

Kdyz se sekta poptavka a nabidka, vznika rovnovaza cena  

Ovlvineno statem, ekonomikou globalni

Podniky:
- male
- strdni
- velke


