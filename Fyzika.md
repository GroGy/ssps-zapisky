#Fyzika
## Hodina 1.
### Kmitavy pohyb

|Co|f/Hz|T/s|
|---|---|---|
|minutova rucicka|$$\frac{1}{3600}$$|1h = 3600s|
|srdce|440|2.3ms|


## Hodina 2.

$$y = y_m*\sin({\omega})*t$$
$$\omega = 2\pi f = \frac{2\pi}{T}$$

$$y = 3 * \sin{\pi} * t$$

## Hodina 5.
Pod jakym uhlem pada paprsek pod lomem zlomu pres material diamant  
n = 2.42  
1.000 = sin{90}  
$$ \beta = \alpha - 90 \degree $$
$$ \alpha = \beta + 90 \degree $$
$$\frac{\sin{\alpha}}{\sin{\beta}} = \frac{n_a}{n_b}$$
$$\frac{\cos{\beta}}{\sin{\beta}} = \frac{n_a}{n_b}$$
$$\tan{\beta} = \frac{n_a}{n_b} = 67.5 \degree$$

## Hodina 6.

$$ R = \rho \frac{l}{s}$$

$$r_2 = \sqrt{\frac{\rho Al}{\rho cn}*{r_1}^2}$$

odstrihneme delku x a dame ji podel zbytku, jak dlouhy musi byt odstrizeny drat jestlize po teto uprave ma klesnou celkovy odpor na polovinu celkeho hodnoty.  

## Hodina 31.

