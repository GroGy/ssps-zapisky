#Cesky Jazyk
## Hodina 1.  
**Otazky**
1. A
2. B
3. C
4. D
5. E
6. F
7. G
8. H
9. I 
10. J
11. K

## Hodina 2.  
*Wednesday, 11. September 2019*
###Alois Jirasek
Stare povesti ceske
Husitstvi
Narodni obrozeni
Dramata -> Historicka, Soucasna

## Hodina 3.  
*Friday, 13. September 2019*
###Pravopis
####Podmet a prisudek
Me zname mi psali dva dopisy.
Dny ubihaly rychle. Dni se kratily dnove prinaseli tady.
Eva a jeji kamarad honza vyjel na Vysocinu.
Napsal obema drivky aby se vratuky domu.
Kam jste se obe vydali tak pozde?

Pripravovali jste se sami?
Deda s babickou na nas cekali.
Prisli k nam jeji kamaradky se psem.

Tancili se svymi hosty.
Kazdou hodinu se na sebe podivali.
Mirove sbory nastoupili do Jugoslavie.
Divky a chlapci se zucastnily souteze.
Stovky lidi hladoveli, a tak dostaly, pomoc od OSN.
Divky s chlapci flirtovali.
Slanecky byli naskladany v konzerve.

## Hodina 4.  
*September 18, 2019*

**Umelecke smery konce 19. stoleti**
Cladue Monet [kled mone] obraz Imprese
**Maliri**  
Degas [defa]
Manet [mane]
Paul Gaugin [pol gogen]
T Cezarnne [sezan]
Renoir [renal]

** Vomismus**  
** Prokleti Basnici**

## Hodina 5.  
*September 25, 2019*  

IM / IN > Dovnitr [importnovat]  
KO / KOM > Dohromady [konstruovat], spoledcne sdileni rysu [kofigurovat]  
KONTRA > Proti [kontraproduktivni]  
PRE > casove pred [premedikace]  
PRO > zastupnictvi [prodekan]  
RE > znovu [regenerovat]  
SUPER > vyssi stupen [superintendant], nadstandardni mira [supergalaxie]  
SYN > [synchornie]  
SU > pod [subordinovat]  
SUPRA > velka mira [supravodivy]  
TRANS > pre [transformovat]  
VICE > zastupnictvi ve spojeni se jmeny osob [vicepremier]  

## Hodina 6.  
*September 27, 2019*

1. Johanna Wolfganga Goethe
2. Puskina, Dostojevskesko, Tolstoje, Turgeneva, Cechova
3. Petra Ilijice Cajkovskeho
4. Gustava Eiffela
5. Halli Machieveleliho
6. Baku, Tsibisi a Jerevanu

### 1.
Problemu > rectina  
Digitalni > latina  
Fotoaparat > recko-latina  
Kapacita >  
problem >  
fotoaparat >  
definitivne > latina  
konstrukteri > latina  
videomobily > latina  
funkci > rectina  

1. jazykovedec
2. skandalni 

## Hodina 7.
*October 11, 2019*

## Hodina 8. 
Zkratky maji 4 pismena:

* Doktor mediciny (MUDr.)
* Doktor veterinarni mediciny (MVDr.)
* Doktor filozofie (PhDr.)
* Doktor pedagogiky (PaedDr.)
* Doktor prav (JUDr.)
* Doktor prirodnich ved (RNDr.)
* Inzenyr (Ing.)
* Bakalar (Bc.)
* Magistr (Mgr.)

Nektere ustalene zkratky jsou tvoreny tak, ze se pisou jen (ne vzdy vsechna) pismena pro souhasky a pismena pro samohlasky se vypousteji  
Napriklad mjr. kpt.  
Zkratky ustalenych slovnich spojeni se pisi dohromady, jako by slo o zkratku slova jednoho  
Napriklad atd. = a tak dale. 
ap.  
apod.  

na rozdil od zkratek se za znackami nepise tecka  
napr. mm (milimetr), kg (kilogram)  
u znacek chemickych prvku se pise pocatecni pismeno vzdy velke  
Napriklad C (Uhlik)  

Mezi cislici a znackou je mezera  
- 10 Km
- 5 %
Mezera vsak neni jestlize se jimi vyjadruje pridavne jmeno
- 50 Km rychlost = padesatikilometrova rychlost  
- 6V baterie = sestivoltova baterie  
- 3% roztok

bez mezerey se pisou tyto vyrazy take v pripadech, kdy je znacka vyjadrena slovne
- 50kilometrova rychlost
- 3procentni roztok
- 60stupnovy uhel apod.

Podobne take piseme:
- 20x = 20krat
- 20nasobny = dvacetinasobny

Inicialove zkratky se pisou bez tecek  
Napriklad nazvy statu > ČR  
Napriklad instituce > OSN  

U cislic ktere oznacuji cislovku radovou > 1.  

## Hodina 9.

**znaky**  

Utvar slohoveho postupu uvahoveho, meli byhom dospet k nejakemu obecnejsimu zaveru, ale nepoucujeme  
objevuje se v umeleckych dilech, publicistice i odbornemu stylu  
druh uvahy na odborne tema oznacujeme jako esej  

**kompozice uvahy**  

logicka stavba - mela by odrazet myslenkovy postup, kterym autor prosel  
nebezpeci predcasne generalizace!  
v uvodu se zamyslime nad cilem uvahy  
ve vlastnim textu premyslime, hodnotime, rozebirame dilci myslenky  
v zaveru shrneme a vyvodime zakladni myslenky uvahy  

## Hodina 10.

### Uvaha
* logicka stavba - mela by odrazet myslenkovy postup, kterym autor prosel
* nebezpeci nevim ceho neco v minule hodine

Skladba  
* logicka navaznost vet
* souveti souradna i podradna
* spojovani otazek a odpovedi
* recnicke otazky
* obraty a podnecujici uvahu  

---
  
* Tema > Muzske a Zenske role  
* Ve kterem veku by mela zacit delba roli  
* Co ji ovlivnuje  

---
   
### Muzske a zenske role

V zivote neexistuji jen pohlavi, ktere jsou muzske a zenske, ale take role, role v ruznych aktivitach, ktere provozujeme, jako je kazdodenni prace, sporty, intimni zivot a dalsi...  
Jedna z hlavnich aktivit, ktera je rozdelena na muzskou a zenskou roli je domaci prace, muzska cast je casto prace na zahrade, starani se o provoz domu, aby bylo teplo, aby neteklo domu kdyz prsi a dalsi tkzv. chlapske prace, na druhe strane jsou zenske prace, to jsou prace, ktere jsou vetsinou mene fyzicky narocne, napriklad uvarit, umyt nadobi nebo napriklad vyluxovat.  
Samotne deleji by se melo zacit v zivote projevovat jiz od detstvi, preci mlady chlapec ma vetsinou mnohem blize k urcitych aktivitam nez divka a naopak, avsak toto tvrzeni neni uplne vseobecne a velice casto se jedna spise podle osobnosti.  
Dalsi dulezity faktor u tohoto tema je i role muze a zeny ve spolecnosti, muz by mel byt gentleman, mel by respektovat zenu, mel by ji pomahat. Na druhe strane zena by mela ukazat muzi ze mu dekuje za jeho respekt.  
V dnesni dobe existuje skupina zen, ktera opovrhuje jinemu pristupu muzu k zenam, je to skupina tkzv. feministek. Ja si osobne myslim, ze jejich nazor neni dulezity, protoze jestlize ve spolecnosti tento pristup fungoval jiz tak dlouho jak funguje, tak by se nemel menit, pokud zena vidi ze ji muz respektuje tak by s tim nemela mit problem.  
Prave spolecnost je jeden z hlavnich faktoru, ktere ovlivnuji rozdelovani roli zeny a muze, mnohem vice v dnesni dobe ovlivni toto tema spolecnost nez nazor jedne osoby, ale v historii tomu tak vzdy nebylo.  
Ja osobne zeny respektuji a snazim se vuci nim chovat slusne a gentlemansky, zenu bych nikdy v zivote napriklad neuderil, coz beru jako vseobecny nerespekt zen.  

## Hodina 11.

Podle stupne odbornosti:  

- vedecke:
  - disertace
- prakticky odborne:
  - popis pracovniho postupu - nejvyssi uroven je vedecke, prace ktere se pisou na vysokych skolach, tkzv. desertacni
- ucebni: 
  - ucebnicovy vyklad

**Typicke jazykove prostredky**  
- neutralni spisovna cestina
- pripadne s prvky kniznimi, napriklad ackoli, jakoy, lze
- Promyslena a propracovana textova kompozice
- Slozita vetna stavba napriklad souveti vyjadrujici vztahy priciny a dusledky
- Zhustena vyjadreni

### Odborny popis
- Presny, uplny, soustredi se na podstatne znaky popisovaneho objektu
- Dulezity je zretel k adresatovi = byva urceno ve zpracovani, tzn. ze predpokladame jeho odborne znalosti a zvolime popis odborny, nebo popularne-naucny
  
**Kompozice**  

- musi mit logiku, rad, system
- je mozne pouzivat vycty nebo cislovani jednotlivych casti
- vyzaduje se velmi presna kompozice

**Jazykove prostredky**  

- spisovny jazyk, precizni
- Pouzivame odbornou teminologii
- VE STATICKYCH POPISECH - casta slovesa mit a bych - nesmeji byt naduzivana
- stylizace je hutna - pouzivame substantiva slovesna nebo dejova misto vedlejsi vety
- souveti by nemela vyt prilis slozita, aby umoznovala snadnou orientaci v popisovanych souvislostech

**Cil odborneho popisu**  

- aby adresat ziskal presnou predstavu o popisovem objektu a zaroven byla naplnena odbornost textu

**Metody zkoumani**  

- Analyza - rozklad na dilci casti
- Synteza - jednotliva fakta se spoji v celek
- Inducke - od jednotlivych jevu k zobecneni
- Dedukce - z obecnych vlastnosti k jednotlivemu jevu teze skupiny
- Teorie - souhrn argumentu
- Analogie - obdoba jevu na zaklade spolecne charackteristiky
- Abstraktce - zobecnovani, zjistovani podstanych vlasnosti
- Pokus a pozorovani - zacatek vyzkumu
- Hypoteza - podlozena domnenka

## Hodina 12.

1. C
2. c
3. c
4. a
5. a
6. c
7. b
8. a
9. a
10. a
11. b
12. b
13. c
14. b
15. c
16. a
17. b
18. a
19. b
20. a
21. c
22. c


## Hodina 13.

**Proza**  
- Romain ROLLAND: predstavitel humanisticky tradice,Nobelova cena za lit.  
- 10 dilny cyklus JAN RZSTOF: roman - reka, o zivpote genialniho nemeckeho hudebnika  

**Protivalecne zamereni - novela**  
- Petr a Lucie
- Zalozena na kontrastu: hruzy valky x vysneny svet milencu, problemy valky x rozvijejici se laska  
- Cas: od ledna do brezna 1918; Pariz  
- Shakespearovsky motiv  

**Tragicky konec - spolecna smrt v kostele pri naletu**  
- Obzaloba nesmyslnosti valky  
- Roman Okouylena duse a Dobryclovek jeste zije = take protivalecny postoj

## Hodina 14.

**Vety podle postoje mluvicho ke skutecnosti**  
Rozdeleni:  

- Oznamovaci
- tazaci: druhy otazek:  
  - zjistovaci > ano / ne (v 7?)
  - doplnovaci > delsi odpoved (v kolik?)
  - vylucovaci > vice moznosti a vybere jednu z nich (v 7 nebo v 8)

- Zadaci:
  - rozkazovaci (Dostanu to konecne?)
  - praci (Mohl bych to uz dostat?)

- Zvolaci:
  - U vsech druhu vet

Zakladni rozdeleni:  

- **vety dvojcelnne** - obsahuji cast podmetovou i prisudkovou - napr. Stromy kvetou.
- **vety jednoclenne** - obsahuji pouze jeden ze zakladnich vetnych clennu, a to prisudek- napr. Prsi.
- **vetne ekvivalenty** - nemaji cast ani podmetovou. ani prisudkovou napr. Lidove noviny

Jednoclenne vety:  

- obsahuji pouze prisudkovou cast, drive byly oznacovany jako jednoclenne vety slovesne
- sloveso ma tvar 3. os. j. c.
- prisudek muze byt holy, rozvity nebo nekolikanasobny
- obvykle vyjadruji prirodni jevy ( V noci se hrozne blyskalo.) nebo telesne a dusevni stavy (boli me v krku.)
- caste jsou jednoclenne vetyt, v nich je prisudek jmenny se sponou (Je tam osklivo. )
- jako jediny mozny podmet do techto vet lze doplnit zajmeno "ono", tim se odlisuji od vet dvojcelnnych s nevyjadrenym podmetem

Vetne ekvivalenty:  

- drive oznacovane jako jednoclenne vety neslovesne
- mohou plnit funkci vety
- podle slovniho druhu, ktery je jejeich zakladem, se rozeluji bla bla nestihl jsem to
- jmenne - napisy na budovach, nazvy knih, casopisu, pozdravy, vyjadreni hdnoceni aj.
- prislovecne - povely, hodnoceni aj.
- casticove - vyjadreni souhlasu nebo nesouhlasu
- citoslovecne - vyjadreni telesnych a dusevnich stavu, hlasu a zvuku

Vetny zapor:  

- jedna se o popreni cele vyty
- urcite sloveso se zaporkou ne-
- zaporne zajmeno / zajmenne prislovce v kombinaci se zapornym slovesem
- zaporka ani v kombinaci se zapornmym slovesem

Clensky zapor:  

- popreni obsahu nekterho vetneho clenu, casti vety
- zaporky ne

Ruseni zaporu:  

- pokud se ve vete potkaji dve ruzne zapory  

Veta jednoducha:  

- veta jednoducha - ma pouze jen jednu zakldani skladebni dvojici
- muze byt hola - obsahuje jen ahole zakladni cleny - nebo rozvita - ma alespon jeden rozvijejici vetny clen Sluunce svitilo x slunce hezky svitilo.  


Ve vyvoji prozy 1. poloviny 20. stoleti jsou tyto proudy:  

- 1. experimentalni proud
- zakladem je naruseni tradicniho pojeti romanu, abstrakce = odklon od objektivniho popisu reality, vyuzivani slozite symboliky a experimentovani
- Franz Kafka, James Joyce, Marcel Proust

<<<<<<< HEAD

- skladebni dvojice - dve slova, ktera k sobe ve vete mluvnicky i vyznamove patri, obvykle tvorena clenem ridicim a zavislym
- zakladni skladebni dvojice - spojeni podmetu a prisudku

- 2. apozic (pristavovaji) - znaceni tehoz jevu dvojim zpusobem
- pristavek byva pripojovan vyrazy a to, tj. a totiz.  

Typy:  
- zpresnujici : uklidila cely byt a to je kuchyn a koupelnu
- vyctu: uklida cely byt, tj. oba pokoje
- sem patri nominativ jmenovaci
- paralelismus: Erzika ptacek, erzika rybka

- jmeno + sloveso + neshodny doplnek 
- primykani

Neco hemingway:  
Nositel nobelovky za starec a more  
neco sandiago  

typek sel lovit, 86 dni nic nechyti, potom chytne velkou rybu, sezerou mu ji zraloci
=======
- 2. tradicni proud - navazuje na tradicni podobu klasickeho romanu
- zakladem je dejova linie, logicka casova posloupnost, autor se vsak vice zameruje na psychickou nevim co protoze to dala do pici
- Ad realisticky proud
- E. Hemingway: Komu zvoni hrana, Starec a more
- Histricke a zivotopisne romany: Lion Fauchtwanger - Goya
- Detektivni romany:  G. Simenon, A. Christie atd.
- Romany se socialni problematikou: Jon Steinbeck - Hrozny hnevu

3. Detabuizujici proud - odkryva dosud nedotknutelkna temata
- Napr v oblasti sexuality Henry Miller: roman Obratnik Raka - otevrene liceni dychtiveho a rozkosnickeho zivota mladeho cloveka; autor obvinen z pornografie, v Parizi vysel az po 2 letech

set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake-modules")
include(cotire)
>>>>>>> adb87f2fc555d1154c1a9473818eae30d040ab8f
