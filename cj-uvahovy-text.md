# Uvahovy text
## Tema: Muzske a zenske role

**Zadaní:**  
Žijeme v moderní době, často slýcháváme mladé lidi říkat, že se cítí být Evropany.  
Úvahový text o tom, jak se změnily mužské a ženské role.  

## Práce

Muž příchází domu ve stejnou dobu jako jeho žena, žena začne vařit a muž během toho poseká zahradu, avšak bylo to mu tak vždy?  
Dříve se muži soustředili převážně na živení rodiny a náročné práce, dnes tomu již tak není, v dnešní době existují případy, kdy byly role muže a ženy vyměněny, nebo pouze přiblíženy, ale proč?  
Hlavním důvodem je fakt, že některé ženy si začaly připadat ukřivděné, že muži jsou duležitější a že jsou nenahraditelní ženami, proto si začali prosazovat svuj názor a rovnost. Dost často se však děje, že ženy chtějí pouze výhody, které obnáší být muž v rodině, ale neuvědomují si, že pokud zrovnopravni muže a ženu, tak že ženy budou zároven nést stejnou zodpovědnost jako muži.  
Dalším důležitým faktem je, že muži přistupují k ženám různě podle kultur, někde se muže k ženám chovají jako gentlemani a ženy jsou za to rády, jinde muži ženami opovrhují a mají je spíše jako domácí služky a pro své potřeby, jinde muži k ženám přistupují neutrálně, nechovají se jako gentlemani, neotevrou jim dveře ani je nepustí sednout v autobuse, ale ženy jsou na to zvykle a neřeší to.  
Problém nastává, když se potkají muži a ženy z jiných kultur, ale proč? Očekává se nějaký přístup. Potom často nastává situace, kdy se nesejdou očekávání a často to neskončí dobře.  
Duležitou část života, která se během vývoje mužských a ženských rolí měnila, byl i intimní život. Ale jak? O tom zde nemohu psát moc detailně, ale jedná se o roli muže a ženy v sexualním životě, jaké se rodí fetiše a jak ovlivňují jejich vztah. Také je důležité zmínit, že dříve byla nahota a sexualní praktiky tabu, nepoužívala se depilace intimních partií a bylo vzácné na časopisech videt ženu ve vyzívavém oblečení.

V dnešní době je názor hodne volný a jelikož tedy každá žena může říct co má na srdci, nemůže mít určenou roli všude stejně, ale je to dobře? Je dobře, že mají ženy volný názor a že nejsou jejich role stanovené obecně? Ano, jednoduchou odpovědí je ano, ale pokud půjdeme dále do detailů, ovědomíme si, že pokud by bylo rozhodnuto, že ženy nechodí do práce a mají menší práva, tak by bylo v domácnosti jasné, kdo má co dělat, ale mohla by vznikout nespokojenost některých žen. Díky volnosti si páry mohou domluvit své role mezi sebou, ženy mají práva pracovat, vzdělávat se a volit.  

Role mužů a žen se budou ještě v naší evoluci hodně vyvíjet a musíme si jen počkat na to, co se bude měnit, můžou se stát věci, které nikdo nečeká, ale to nikdo neví.
