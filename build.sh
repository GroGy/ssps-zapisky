#!/bin/bash

while read a;do pandoc -H style.pandoc $a -o pdf/$( echo $a | awk -F. '{print $1}').pdf; done < <(ls)

echo done
