# SWOT

|Strengths|Weaknesses|Opportunities|Threats|
|---|---|---|---|
|Unikátnost produktů|Falešné produkty z venku|Párty pro úspěšné podnikatele|Existují již zažité firmy|
|Limitovaná kvantita produktů|Zatím v nás lidé nevidí cenu|Předvádění produktů u osob vázaných na téma|Vracení produktů z důvodu nespokojenosti|
|Kvalitní struktura produktů|Na začátek drahé na provoz|||
|Targetování více cenových skupin (spíše pro bohaté)||||
|Stylové balení představující vitrínu||||