# Karel Gott
## Slohová práce
## Matěj Vrba

Zdravim všechny posluchače a posluchačky,dnes vám zde něco řeknu o padlém hrdinovi, o muži, který nás v nedávné době opustil a který pro nás udělal více, než velká většina lidí, které známe.  
Dnes budu mluvit o Karlu Gottovi,o skvělém zpěvákovi, herci a o správném muži.  
Karel Gott se narodil před polovinou 20. století v Plzni, jeho velkým snem v mládí bylo stát se malířem, ale bohužel (možná bohudík) neúspěl a šel studovat jako elektromontér. Během totoho studia začal rozšiřovat svou druhou velkou zálibu, neboli zpěv.  
Aby se Pan Gott mohl rozvíjet ve zpěvu, odešel na konzervatoř, kde začal svou zálibu rozšiřovat a začal se v ní zlepšovat.
V následujícíh pár letech (od 50. let), začal zpívat v klubech a začal se pokoušet o dalších pár soutěží.
V roce 1958 se umístil na 1. místě v pěvecké souteži amatérských zpěváků, díký které o sobě dal dále vědět a začal zpívat v jestě více kavárnách.  
V Roce 1962 udělal duet s Vlastou Průchovou, jehož popuklrita vystřelila raketově, "Až nám bude dvakrát tolik."
V roce 1962 následovně dostal nabídku na to, aby hrál v divadle Semafor, což v jeho životě byla velice důležitá chvíle.
V roce 1965 odešel z divadla Semafor a po dvou letech vydal jeden ze svých nejlepších "songů", "Trezor".
Rokem 1968 se zůčastnil za Rakousko souteže Eurovision Song Contest.
Zde získal 13. místo a díky tomuto úspěchu se dostal do podvědomí spoustě posluchačů a započal tím svou zahraniční kariéru.
Jeho nejlepší, takzvaně "Zlatá éra" proběhla v 80. a 90. letech 20. století, dostal v tomto období spoustu ocenění a napsal své nejlepší hity.  
Bohužel mu během roku bylo diagnostikováno onemocnění, kterému 1. Října tohoto roku podlehl.  
Chtěl bych všem posluchačům poděkovat za poslouchání dnešního vysílaní a nashledanou.