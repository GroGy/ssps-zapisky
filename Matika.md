# Matematika

## Hodina 2.
*Wednesday, 11. September 2019*  
### Komplexni cislo zdruzene

#### Priklad 1.
$$\frac{Z_{2}}{Z_{1}}$$ 
$$\frac{3+i}{5+3i}*\frac{5+3i}{5+3i} $$

#### Priklad 2.
$$\frac{5}{-2+3i}$$
$$\frac{5}{-2+3i}*$$

## Hodina 6.
$$Z^m*Z^n =  Z^{m+n}$$
$$(Z_1*Z_2)^m = {Z_1}^m*{Z_2}^m$$
$$(Z^m)^n = Z^{m*n}$$

|i^n|result|
|---|---|
$i^1$|i|
|$i^2$|-1|
|$i^3$|-i|
|$i^4$|1|
|$i^5$|i|
|$i^6$|-1|
|$i^7$|-i|
|$i^8$|1|
|$i^9$|i|

**Princip**  

|n %  4|result|
|---|---|
|0|1|
|1|i|
|2|-1|
|3|-i|

## Hodina 7.  
*Monday, 16. September 2019*

$$\frac{x}{2+3i} = 5 + 6i  / * {2+3i}$$
$$x =(5+6i)*(2+3i)$$

## Hodina 8.  
*Tuesday, 17. September 2019*

**Gaussova rovina**    
$$|z| = \sqrt{a^2 + b^2}$$

## Hodina 11.  
### Goniometricky tvar D

$$ Z_1 = a + b_i$$
$$ z_1 = cos(uhel)*d + (sin(uhel)*d)_i$$

## Hodina 12.
|uhel|sin|cos|
|--|--|--|
|0|$0$|$1$|
|$\frac{\pi}{6}$|$\frac{1}{2}$|$\frac{\sqrt{3}}{2}$|
|$\frac{\pi}{4}$|$\frac{\sqrt{2}}{2}$|$\frac{\sqrt{2}}{2}$|
|$\frac{\pi}{3}$|$\frac{\sqrt{3}}{2}$|$\frac{1}{2}$|
|$\frac{\pi}{2}$|$1$|$0$|

$$ z_1 = \cos{\frac{\pi}{6}} + i * \sin{\frac{\pi}{6}}$$
$$ z_1 = \frac{\sqrt{3}}{2} + \frac{1}{2}i$$  
- - -
$$ z_2 = 3*(\cos{\frac{\pi}{3}} + i * \sin{\frac{\pi}{3}}) $$
$$ z_2 = 3*(\frac{1}{2} + \frac{\pi}{2}i) $$
$$ z_2 = \frac{3}{2} + \frac{3\pi}{2}i$$
- - -
$$ z_3 = 4*(\cos{\frac{5}{6}\pi} + i* \sin{\frac{5}{6}\pi}) $$

$$ z_3 = 4*(- \frac{\sqrt{3}}{2} + \frac{1}{2}i)$$
$$ z_3 = \frac{4*\sqrt{3}}{2} + 2i $$
$$ z_3 = 2 * \sqrt{3} + 2i $$
- - -

$$ z_4 = 6 * (\cos{\frac{5}{3}\pi} + i* \sin{\frac{5}{3}\pi}) $$
$$ z_4 = 6 * (-\frac{\sqrt{3}}{2} + \frac{1}{2}i)$$
$$ z_4 = 3*\sqrt{3} + 3i $$

- - -

$$\textbf{z = |z| * (cos{R} * sin{R})}$$
- - -

## Hodina 13.

$$ z_1 = 5*(\cos{\pi} + i * \sin{\pi}) $$
$$ z_1 = 5 (-1 + 0i) $$
$$ z_1 = -5 + 0i $$
- - -

$$ z_3 = \cos{\frac{\pi}{3}} + i * \sin{\frac{\pi}{3}} $$
$$ z_3 = \frac{1}{2} + \frac{\sqrt{3}}{2}i $$

**Pokud je absolutni hodnota v minusu tak to nejde**  
**Pokud se nerovna R u sin a cos tak to nejde**  
**Pokud je prvni sinus a druhy cosinus tak to nejde**  
**Pokud je minus mezi sinus a cosinus tak to nejde**  
**Pokud neni i tak to nejde**  

$$ z_1 = -7i $$
$$ z_1 = 0 - 7 * i $$
$$ z_1 = 7 * (0 + i * 1) $$

## Hodina 15.
### Goniometricky tvar $z \in C$


$$
\frac
{3 + 15i}
{2 - 3i}
*
\frac
{2 + 3i}
{2 + 3i}
$$

$$
\frac
{6 + 9i + 30i - 45}
{4 + 9}
$$

$$
\frac
{-39+39i}
{13}
= 
-3 + 3i
$$

$$
z_1 = x*(\cos{(\arccos{-1})}*i*\sin{(\arcsin{1})})
$$

$$
z_1 = x*(\cos{\frac{3\pi}{4}}*i*\sin{\frac{3\pi}{4}})
$$

$$ |z_1| = \sqrt{-3^2 + 3^2} $$
$$ |z_1| = \sqrt{18}$$
$$ |z_1| = \sqrt{2}*\sqrt{9}$$
$$ |z_1| = 3\sqrt{2}$$
$$ x = |z_1| $$
$$ 3\sqrt{2}*(\cos{\frac{3\pi}{4}}*i*\sin{\frac{3\pi}{4}}) $$

- - -
$$
 \frac
{1+2i}
{2-i}
*
\frac
{2+i}
{2+i}
$$

$$
\frac
{2 + i + 4i - 2}
{4 + 1}
$$

$$
\frac
{5i}
{5}
$$

$$
z_1 = 0 + i
$$
$$z_1 = \cos{\frac{\pi}{2}}*i*\sin{\frac{\pi}{2}}$$
- - - 
$$ z_3 = 5*(\cos{\pi}+i*\sin{\pi}) $$
$$ z_3 = 5*(-1)$$
$$ z_3 = -5$$

## Hodina 16.
*October 1. 2019*  
### Nasobeni a deleni komplexnich cisel v goniometrickem tvaru

$$ z_1 = r_1 * (\cos{\varphi_1} + i * \sin{\varphi_1})$$
$$ z_2 = r_2 * (\cos{\varphi_2} + i * \sin{\varphi_2})$$
$$
z_3 =
z_1*z_2 =
r_1 * r_2 * (\cos{(\varphi_1+\varphi_2)} + i * \sin{(\varphi_1+\varphi_2)}) $$

$$
z_3 =
\frac{z_1}{z_2} =
\frac{r_1}{r_2} *(\cos{(\varphi_1-\varphi_2)}+i*\sin{(\varphi_1-\varphi_2)})
$$

**Dulezita vec**
$${z_1}^n = {r_1}^n * (\cos{n*\varphi_1} + i *\sin{n*\varphi_1})$$
**Priklady**

$$
z_1 = 0.5 * (\cos{\frac{11\pi}{6}}+i*\sin{\frac{11\pi}{6}})
$$

$$
z_2 =  4 * (\cos{\frac{2\pi}{3}}+i*\sin{\frac{2\pi}{3}})
$$

$$ z = 2 * (\cos{\frac{\pi}{2}}+i*\sin{\frac{\pi}{2}})$$

$$ z = 2i $$

## Hodina 17.
### Reseni kvadratickych rovnic v C
- - -
$$6x^2 - 19x + 15 = 0$$
$$D = 361 - 360 $$
$$D = 1$$

$$x_{1,2} =
\frac{-b \pm \sqrt{D}}{2a} $$

- - -

$$5x^2 - 6x + 2 = 0$$

## Hodina 18.
$$ 4x^2 - 16x + 25 = 0 $$

## Hodina 19.
$$ -9x^2 + 13x -10 = 0 $$
$$ D = 169 - 360 $$
$$ D = - 191 $$
$$ x_{1,2} = \frac{13 \pm i \sqrt{191}}{18} $$
$$ x_{1,2} = \frac{13}{18} \pm \frac{\sqrt{191}}{18}i $$

$$ 6x^2 + 14x + 10 = 0 $$
$$ D = 196 - 240 $$
$$ D = -44 $$
$$ x_{1,2} = \frac{14 \pm i \sqrt{44}}{12} $$
$$ x_{1,2} = \frac{7}{6} \pm \frac{\sqrt{44}}{12}i $$
$$ x_{1,2} = \frac{7}{6} \pm \frac{\sqrt{11} * 2}{12}i $$

## Hodina 20.

### Binomicke funkce

$$ x^n - a = xn - |a|*(\cos{\alpha} + i \sin{\alpha})$$
$$ x_k = n\sqrt{|a|}*(\cos{\frac{\alpha * 2k\pi}{n}} + i* \sin{\frac{\alpha * 2k\pi}{n}}) $$

$$ x^4 - 1 = 0 $$
$$ x^4 = 1 $$
$$ 4 \sqrt{1} * (\cos{\frac{0 + 2k\pi}{4}} + i * \sin{\frac{0 + 2k\pi}{4}}) $$
$$ k = 1*(\cos{0} + i* \sin{0}) => x_0 = 1 + 0i$$ 

## Hodina 21.

$$x^9 = \sqrt{8} + i \sqrt{8}$$
$$x_0 = ^4\sqrt{2} * (\cos{\frac{\alpha*2\pi*k}{}})$$

## Hodina 22.
*Test*  
## Hodina 23.

$$z^2 = 9i$$
$$z^2 - 9i = 0$$

$$ x_k = \sqrt{|a|} * (\cos{\frac{\alpha + 2k\pi}{n}} + i * \sin{\frac{\alpha + 2k\pi}{n}})$$

$$ \alpha = \frac{\pi}{2}$$

$$ z_0 = \sqrt{9} * (\cos{\frac{\frac{\pi}{2} + 0}{2}} + i*\sin{\frac{\frac{\pi}{2} + 0}{2}}) $$

$$ z_0 = 3 * (\frac{\sqrt{2}}{2}+i\frac{\sqrt{2}}{2})$$
$$ z_0 = \frac{3\sqrt{2}}{2} + i \frac{3\sqrt{2}}{2}$$

- - -

$$ z_1 = \sqrt{9} * (\cos{\frac{\frac{\pi}{2} + 2\pi}{2}} + i*\sin{\frac{\frac{\pi}{2} + 2\pi}{2}}) $$

$$ z_1 = 3 * (\frac{5\pi}{4}+i\frac{5\pi}{4})$$
$$ z_1 = 3 * (- \frac{\sqrt{2}}{2} + - i\frac{\sqrt{2}}{2})$$
$$ z_1 = -\frac{3\sqrt{2}}{2} - i \frac{3\sqrt{2}}{2}$$

---
---

$$ z^4 - 16 = 0$$
$$ \alpha = 0 $$

$$ z_0 = \sqrt{16} * (\cos{\frac{0 + 0}{2}} + i\sin{\frac{0 + 0}{2}})$$
$$ z_0 = \sqrt{2}*(1 + 0i) $$
$$ z_0 = \sqrt{2} + 0i $$

## Hodina 31.

body se znaci:  
- Velke pismeno
- Hranate zavorky "[]"

A[1;2]

$$
\sqrt{(x_A - x_B)^2 + (y_A - y_B)^2}
$$

A[-2;2]  
B[4;5]  
C[5;-2]  
D[5;3]  

## Hodina 32.

A[5,-8]
B[10,15]

$$|S_{AB}| = [\frac{5+10}{2},\frac{15-8}{2}]$$
$$|S_{AB}| = [7.5,3.5]$$

### Vektor

$A[a_x,a_y]$  
$B[b_x,b_y]$  

## Hodina 33.
~~nebyl~~  
## Hodina 34.
~~nebyl~~    
## Hodina 35.
### Linearni zavislost vektoru
Spocitame koeficient u kazde souradnice  

$$ v_1(-1,3,6)$$
$$ v_2(2,6,-12)$$

$$ v_x = k_x * u_x $$
$$ -1 = k * 2$$
$$ k = \frac{-1}{2}$$

Je to v linearni souvislosti pokud jsou koeficienty vsech os (X,Y,Z) stejne!  

Vektory u,v,z jsou LZ prave tehdy kdyz 
$$ u = k * v + l * z $$
$$ k,l \in \mathbb{R}$$

$$ \vec{u}(2,3,1) $$
$$ \vec{v}(2,3,5) $$
$$ \vec{z}(0,6,-4) $$

$$ 2 = k*2 + l* 0 : k = 1, l = 0$$
$$ 3 = k*3 + l* 6 : k = 1, l = 0$$
$$ 1 = k*5 + l*-4 : k = 1, l = 1$$

### Hodina 36.

$$ \vec{u}(3,5,4) $$
$$ \vec{v}(6,1,-2) $$
$$ \vec{z}(0,9,10) $$

$$ 3 = k*6 + l* 0 : k = \frac{1}{2}, l = \frac{1}{2} $$
$$ 5 = k*1 + l * 9 : k =\frac{1}{2}, l = \frac{1}{2} $$
$$ 4 = k*-2 + l*10 : k =\frac{1}{2}, l = \frac{1}{2} $$

$$ \vec{u}(u_x,u_y,u_z)$$
$$ \vec{v}(v_x,v_y,v_z)$$
$$ \vec{w}(w_x,w_y,w_z)$$

$$
\quad
\begin{bmatrix}
u_x & u_y & u_z \\
v_x & v_y & v_z \\
w_x & w_y & w_z \\
\end{bmatrix}
$$

$$ x * +1 = u_x * v_y * w_z $$
$$ y * +1 = v_x * w_y * u_z $$
$$ z * +1 = w_x * u_y * v_z $$

$$ Vysledek+ = x + y + z$$

$$ x * -1 = u_z * v_y * w_x $$
$$ y * -1 = v_z * w_y * u_x $$
$$ z * -1 = w_z * u_y * v_x $$

$$ Vysledek- = x + y + z$$

$$ D = (Vysledek+) + (Vysledek-)$$

**Pokud D = 0 tak jsou LZ jinak jsou LNZ**

$$ 50 + -280 + 6 = -214 $$
$$ -8 - 42 + 300 = 250 $$
LNZ  

## Hodina 37.
~~nebyl~~
## Hodina 38.
~~nebyl~~
## Hodina 39.
$$\vec{u}(5;4;-3)$$
$$\vec{v}(-2;3;9)$$
$$x = u_x * v_x$$
$$y = u_y * v_y$$
$$z = u_z * v_z$$
$$Vysledek = x + y + z$$
$$ \vec{u}*\vec{v} = -1 $$

### Odchylka vektoru
Delka vektoru:  
$$Delka = \vec{u}(5,4,-3)$$
$$Delka = \sqrt{5^2+4^2+-3^2}$$
$$Delka = \sqrt{25+16+9}$$
$$Delka = \sqrt{50}$$

$$\vec{u} * \vec{v} = |u|*|v|*\cos{\phi}$$

$$\frac{\vec{u}*\vec{v}}{|u|*|v|} = \cos{\phi}$$

$$\frac{-1}{\sqrt{50} + \sqrt{14}} = \cos{\phi}$$

$$\frac{-1}{10 + \sqrt{7}} = \cos{\phi}$$

## Hodina 40.
### Cross product (vektorovy soucin)

$$ \vec{u} \not ={k*\vec{u}; k \in \mathbb{R}} $$

Vystup vekteroveho soucinu vektoru je vektor  
$$\vec{w} = \vec{u}  x  \vec{v}$$

$$ w_x = (u_y * v_z) - (u_z * v_y)$$
$$ w_y = (u_z * v_x) - (u_x * v_z)$$
$$ w_z = (u_x * v_y) - (u_y * v_x)$$

## Hodina 41. 
$$ C[1;2] $$ 
$$ A[5;3] $$

$$ \vec{v}(4;-1) $$
$$ S_{\vec{v}} = \sqrt{(16) + (1)}$$
$$ S_{\vec{v}} = \sqrt{17} $$

## Hodina 42.

~~nebyl~~

## Hodina 43.

### Parametricka rovnice primky  

$$A[A_x,A_y],B[B_x,B_y]$$  
$$\vec{u}(B_x - A_x, B_y + A_y)$$  
$\vec{u}$ je smerovy vektor primky AB  
Priklad:    
$$A[2,4],B[9,6]$$  
$$\vec{u} = (9 - 2; 6 - 4) = \vec{u}(7;2)$$  
Kazda primka ktera sdili tento smerovy vektor je rovnobezna    

$$ p :  A + k\vec{u} ; k \in \mathbb{R}$$  

Parametricka rovnice primky:  
$$ p : x = 2+ k*7 $$  
$$ p : y = 4+ k*2 $$  

$$ C[18,8] $$  
$$ 7 = k * 18 $$  
$$ 2 = k * 8 $$  

nejde protoze 18 * $\frac{1}{4}$ neni 7!  

$$ N[16;8] $$  
$$ 16 = k * 7  +  2 ; - 2$$  
$$ 8 = k * 2  + 4 ; - 4$$  

## Hodina 44.  

$$ \vec{u}(3;-2) $$  
$$ p : x = k * 3 + 6 $$  
$$ p : y = k * -2 + 6 $$  
$$ k \in \mathbb{R} $$  

$$ \vec{u_1}(1,3) $$
$$ \vec{u_2}(3,-1) $$
$$ 6 + (-6) = 0 $$

$$C \in q \wedge q \perp p$$
$$q_1 :x = C_x + k * -2 $$
$$q_1 :y = C_y + k * 3 $$
$$k \in \mathbb{R}$$

pokud chci prusecik tak $6 + 2k  = 3 + 3l$ a $6 + 3k  = 3 - 2l$

## Hodina 45.

$$ A[-100;4] $$
$$ B[200;60] $$
$$\vec{u}(300;56)$$
$$\vec{v}(56;-300) $$
$$\vec{v}(14;-75) $$
$$a*14 + b*-75 + c = 0 $$
$$-100*14 + 4*-75 + c = 0 $$
$$ c_1 = -1400 -  300 $$
$$ c_1 = -1700 $$ 

$$c_2 = 2800 - 4500$$

$$x*14 + y*-75 - 1700 = 0 $$

$$ p = 2x - 3y + 6 = 0 $$
$$ \vec{v}(  2 ; -3 ) $$
$$ \vec{u}( -3 ; -2 ) $$

$$p : 4x - y + 6 = 0 $$
$$ \vec{u_p}(4,-1) $$
$$ \vec{v_p}(-1,-4) $$

$$ -x - 4y + c = 0 $$
$$ 5 - 40 + c = 0 $$
$$ -35 + c = 0 $$
$$ c = 35 $$

$$ 4x + -y + 6 = 0 $$
$$ -x - 4y + 35 = 0 $$

$$ 4x - y + 6 = 0 $$
$$ 4x - 16y + 140 = 0 $$

## Hodina 49.

### Vzajemna poloha dvou primek urcenych paratrickou rovnici

$$ p: 3x + 2y - 4 = 0 $$
$$ q: p || q \And M[6;-2] \in q $$
$$ o: o \perp p \wedge M \in o $$

$$ \vec{u}(-2;3) $$

$$ o : -2x + 3y + c = 0 $$
$$ -12 - 6 + c = 0 $$
$$ -18 + c = 0 $$
$$ c = 18 $$

$$ o = -2x + 3y + 18 = 0 $$

---

$$A[-4,-3]$$
$$B[3,4]$$
$$C[4,-1]$$
$$p: A,B \in p $$
$$ C \in q || p$$

$$A[2,3]$$
$$B[5,2]$$
$$C[5,6]$$

$$ p: -x + y = 2 $$
$$ A $$

$$ -x = 2 - y$$
$$ x = -2 + y $$

$$ \vec{u}[-1,1] $$
$$ \vec{p}[1,1] $$

$$ p_a : x*1 + y*1 + c = 0 $$
$$ p_a : 2*1 + 3*1 - 5 = 0 $$

$$ p_{Ax} : x = 5 - y $$
$$ p_{Ax} : x = - ( 5 - y) + y = 2 $$
$$ p_{Ax} : x = -5 + 2y = 2 $$
$$ p_{Ax} : x = -7 = -2y $$
$$p_{Ax} : x = \frac{7}{2} $$

$$ p_b : x*1 + y*1 + c = 0 $$
$$ p_b : 5*1 + 2*1 - 10 = 0 $$

$$ p_c : x*1 + y*1 + c = 0 $$
$$ p_c : 5*1 + 6*1 - 11 = 0 $$

## Hodina 51.

$$ \vec{u}(2,-3)$$
$$ \vec{u}(4,2)$$

$$\frac{(2 * 4)*(-3 * 2)}{\sqrt{2^2+(-3)^2 * \sqrt{4^2 + 2^2 }}}$$

$$\frac{1}{\sqrt{13} * \sqrt{5}} = 0,12 = 83\degree $$

$$\vec{u}(6,10) $$
$$\vec{v}(3,-1) $$

$$\frac{(6*3) + (10*-1)}{\sqrt{6^2 + 10^2} * \sqrt{3^2 + (-1)^2}}$$
$$\frac{8}{\sqrt{36 + 100} * \sqrt{9 + 1}}$$
$$\frac{8}{\sqrt{136} * \sqrt{10}}$$
$$\frac{4}{\sqrt{39} * \sqrt{10}} = 0,21 = 77\degree$$

## Hodina 52.

$$ y = k*k + q $$
 q je posun na ose Y
$$ 5x + 6y - 8 = 0 $$
$$ x = 4-6k $$
$$ y = -2+5k $$

$$ 6y = -5x + 8 $$

$$ y = \frac{-5x}{6} + \frac{4}{3} $$

$$ y = \frac{4}{3} $$
$$[0,\frac{4}{3}]$$

$$A[-7,1]$$
$$B[2,2]$$

- - -

$$\vec{v}(9;1) $$
$$\vec{u}(1;-9) $$

$$p: x = -7 + k$$
$$p: y = 1 - 9k$$

$$ x - 9y + c = 0 $$
$$ 2 - 18 + c = 0$$
$$ c = 16$$

$$ -9y + 16 = 0 $$

## Hodina 53.

$$ A[3, -5, 7]$$
$$ B[-4,3,8] $$
$$ C[0,1,2] $$
$$ p \in A,B $$
$$ q: p || q AND q \in C $$

$$ \vec{v}(7,-8,-1) (A - B)$$

Abych nasel kolmy vektor musim najit takovy vektor aby soucin byl 0, dosadim 2 moje cisla  
$$ 0 = 7 * w_x + -8 * w_y - 1 * w_z $$
$$ p: x = 0 + 1l $$
$$ p: y = 1 $$
$$ p: z = 2 + 7l $$

$$ D = C + l * \vec{w} $$

## Hodina 54.

$$\rho = A +\vec{u}*k + \vec{v}*l $$

$$ x = A_x + \vec{u}_x*k + \vec{v}_x*l$$  
$$ y = A_y + \vec{u}_y*k + \vec{v}_y*l$$  
$$ z = A_z + \vec{u}_z*k + \vec{v}_z*l$$  

$$A[1,2,3]   $$
$$B[2,-1,3]  $$
$$C[10,11,3] $$

$$\vec{C} = C - A$$
$$\vec{C} = 9,9,0 $$
$$ 2 = 10 + 9k $$
$$ -1 = 11 + 9k $$
$$ 3 = 3 + 0k $$

$$ -8 = 9k $$

$$ x = 8 + 9k $$
$$ y = 12 + 9k $$
$$ z = 0 $$

WTF  

A[0,0,0]  
B[4,2,-1]  
C[2,-2,3]  

$$\vec{C} = 2,-2,3 $$
$$\vec{B} = 4,2,-1 $$

$$ x = 0 + 2k + 4l $$
$$ y = 0 - 2k + 2l $$
$$ z = 0 + 3k - 1l $$

## Hodina 53.

$$\tiny \vec{C}_x  \normalsize \vec{C}_y \vec{C}_z \vec{C}_x \vec{C}_y \tiny \vec{C}_z $$
$$\tiny \vec{B}_x  \normalsize \vec{B}_y \vec{B}_z \vec{B}_x \vec{B}_y \tiny \vec{B}_z $$
$$\vec{w}_x = ( \vec{C}_y * \vec{B}_z ) - (\vec{C}_z * \vec{B}_y) $$
$$\vec{w}_x = ( \vec{C}_z * \vec{B}_x ) - (\vec{C}_x * \vec{B}_z) $$
$$\vec{w}_x = ( \vec{C}_x * \vec{B}_y ) - (\vec{C}_y * \vec{B}_x) $$

Parametricka rovnice roviny  

$$ax + by + cz + d = 0$$
Za abc dosadim normalovy vektor  
$$ 4x - 14y - 12z + d = 0 $$
$$ 0 - 0 - 0 + d = 0 $$
$$ d = 0 $$
$$ 16 - 28  + 12 = 0 $$
$$ 8 + 28 - 36 = 0   $$

$$ M[5,5,5] $$
$$ N[1,2,3] $$
$$ O[6,4,-2] $$

$$ \vec{u} = -4,-3,-2 $$
$$ \vec{v} = 1,-1,-7 $$

$\vec{w}$ = Normala  

$$ w_x = (-3 * -7) - (-2 * -1) $$
$$ w_y = (-2 * 1 ) - (-4 * -7) $$
$$ w_z = (-4 * -1) - (1 * -3) $$

$$ \vec{w} = 19,-30 , 7 $$

$$ 19x - 30y + 7z + d = 0 $$

$$19 - 60 + 21 + d = 0$$
$$ d = 20 $$
$$ 19x - 30y + 7z + 20 = 0$$

## Hodina 58.

$$ x = 3 + 2r - 3t $$
$$ y = -1 - r + 3t $$
$$ z = 4 + 3r - 2t $$

$$ \vec{u}(2,-1,3) $$
$$ \vec{v}(-3,3,-2) $$

$$ \vec{u} = k * \vec{v} $$

$$ \vec{u}_x : 2 = k*(-3)$$
$$ \vec{u}_y : -1 = k*(3)$$
$$ \vec{u}_z : 3 = k*(-2)$$

$$ k = - \frac{2}{3}$$

$$ \vec{n}(-7,-5,3) $$

$$ A[1,2,3] $$
$$ B[-1,-2,-3] $$
$$ C[5,5,5] $$

## Hodina 59.

$$ A[4,4,2]$$
$$ B[-3,-3,-6]$$
$$ C[-5,-5,-20]$$

$$ D[4,-4,-3] $$
$$ E[-2,2,-9] $$
$$ F[0,0,-10] $$

$$ A - B = \vec{u}(7,7,8) $$
$$ A - C = \vec{v}(9,9,22) $$

$$ D - E = \vec{m}(6,-6,6) $$
$$ D - F = \vec{n}(4,-4,7) $$

$$ \vec{x} = normala- ABC $$
$$ \vec{y} = normala- DEF $$

$$\vec{x}_x = (7*22) - (8*9) $$
$$\vec{x}_y = (8*9) - (7*22) $$
$$\vec{x}_z = (7*9) - (9*7) $$

$$\vec{x}(82,-82,0)$$

$$\vec{y}_x = (-6*7) - (-4*6) $$
$$\vec{y}_y = (6*4) - (7*6) $$
$$\vec{y}_z = (6*-4) - (-6*4) $$

$$\vec{y}(-18,-18,0)$$

$$\vec{x}_x = k*\vec{y}_x$$
$$\vec{x}_y = k*\vec{y}_y$$
$$\vec{x}_z = k*\vec{y}_z$$

k neexistuje  

Scalar:  
$$ (\vec{x}_x * \vec{y}_x) + (\vec{x}_y * \vec{y}_y) = 0 $$

## Hodina 60.
$$ C = B + a $$
$$ C = 11,-6 $$

## Hodina 61.

a. 

## Hodina 63.

### Polohove ulohy v 3D prostoru

Rovina -> normalovy vektor  
Primka -> smerovy vektor  

Poloha primky x poloha Rovina  

Udelam 90° - ( odchylku smeroveho vektoru primky a normaloveho vektoru roviny )  

$$ A[3,2,1], $$

$$p: x = 4 - 3k $$
$$p: y = 2 + k $$
$$p: z = -3 - 2k $$

$$\vec{u} = B - A$$
$$\vec{u} = -5,-2,0 $$
$$\vec{v} = -3,1,-2 $$

$$ 90° - \arccos{(\frac{\vec{u}*\vec{v}}{|\vec{u}|*|\vec{v}|})}$$
$$ 90° - \arccos{(\frac{13}{\sqrt{29}*\sqrt{14}})}$$

$$90° - 49° 50' $$
odchylka = $40° 10'$  

$$ 3x - 2y + 3z + 2 = 0 $$
$$ 4x + y - 3z + 1 = 0 $$

$$\vec{v}(3, -2,  3 )$$
$$\vec{u}(4,  1, -3 )$$

$$ \cos{\varphi} = \frac{\vec{u}x\vec{v}}{|\vec{u}|*|\vec{v}|}$$

## Hodina 64.

$$ T[3,-2,6] $$
$$ \rho = 4x + y -3z + 3 = 0 $$

$$ \vec{u} = 4,1,-2$$

$$ p:x = 3 + 4k $$
$$ p:y = -2 + k $$
$$ p:z = 6 - 2k $$

DOPLNIM PARAMETRICKOU DO OBECNY ROVINY  

$$ 4*(3+4k) + (-2 + k) -3* (6 - 2k) + 3 = 0 $$

$$ 12 + 16k - 2 + k -18 + 6k +3 = 0 $$
$$ 1 + 21k = 0 $$
$$ k = - \frac{1}{21} $$

$$ P_x = 3 + 4(-\frac{1}{21})$$
$$ P_x = \frac{63}{21} - \frac{4}{21}$$
$$ P_x = \frac{59}{21}$$

$$ P_y = -2 -\frac{1}{21}$$
$$ P_y = -\frac{42}{21} - \frac{1}{21}$$
$$ P_y = -\frac{43}{21}$$

$$ P_z = 6 - 2(-\frac{1}{21})$$
$$ P_z = \frac{126}{21} + \frac{2}{21}$$
$$ P_z = \frac{128}{21}$$

$$P[\frac{59}{21}, -\frac{43}{21}, \frac{128}{21}] | P \in \mathbb{R}$$  

VZDALENOST:  

$$P - T  = (\frac{-4}{21},\frac{-1}{21},\frac{2}{21})$$
$$ \sqrt{\frac{-4}{21}^2-\frac{1}{21}^2+\frac{2}{21}^2}$$
$$ \sqrt{\frac{16}{441}+\frac{1}{441}+\frac{4}{441}}$$
$$ \sqrt{\frac{21}{441}}$$
$$ \frac{\sqrt{21}}{21} = 0.22 $$


## Hodina 65.

$$p: x = 1 - 2k$$
$$p: y = 1 - k$$
$$p: z = -2 - 8k$$

$$q: x = 8 + 5t $$
$$q: y = 9 + 7t $$
$$q: z = 1 - 7t $$

$$ x: 8 + 5t = 1 - 2k $$
$$ y: 9 + 7t = 1 - k $$
$$ z: 1 - 7t = -2 - 8k $$

Vyberu si 2 z nich, vypocitam z nich k a t a potom ho dosadim do te 3.  

$$ 5t = 1 - 2k - 8 $$
$$ 5t = -7 - 2k $$
$$ t = \frac{-7 - 2k}{5}$$

$$ -k = 8 + 7t $$
$$ k = -8 - 7t $$

$$ 1 - 7(\frac{-7 - 2k}{5}) = -2 - 8k $$
$$ 1 - (\frac{-49 - 14k}{5}) = -2 - 8k $$
$$ 1 + \frac{49 + 14k}{5} = -2 - 8k $$
$$ \frac{5}{5} + \frac{49 + 14k}{5} = \frac{-10}{5} - \frac{-40k}{5} $$
$$ 5 + 49 + 14k = -10 - 40k $$
$$64 = -24k $$
$$ 24k = -64 $$
$$ 3k = -8 $$
$$ k = - \frac{-8}{3} $$
Neni dobre  

$$ p: x = 2 + 3k $$
$$ p: y = 3 - k $$
$$ p: z = 1 + 2k $$

$$ q: x = 2 - 3t $$
$$ q: y = 6 + 2t $$
$$ q: z = 8 + 3t $$

$$ 2 + 3k = 2 - 3t $$
$$ 3 - k = 6 + 2t $$
$$ 1 + 2k = 8 + 3t $$




$$T[0,0,0]$$
$$ q: x = 2 - 3t $$
$$ q: y = 6 + 2t $$
$$ q: z = 8 + 3t $$

Urci vzdalenost bodu T od primky p  

Smerovy vektor primky v 3D prostoru pouziju jako normalovy vektor roviny na ktere lezi bod T  

$$ -3x + 2y + 3z + d = 0 $$

Dosadim bod T  

$$ -3x + 2y + 3z = 0 $$

Dosadim z parametricke rovnice do obecne roviny  

$$-3(2 - 3t) + 2(6 + 2t) + 3(8 + 3t) = 0 $$
$$ -6 + 9t + 12 + 4t + 24 + 9t = 0 $$

$$ 30 + 22t = 0 $$
$$ 22t = -30 $$
$$ 11t = -15 $$
$$ t = \frac{-15}{11} $$


$$ P_x = 2 - 3(\frac{-15}{11}) $$
$$ P_y = 6 + 2(\frac{-15}{11}) $$
$$ P_z = 8 + 3(\frac{-15}{11}) $$

$$ P_x = 2 +\frac{45}{11} $$
$$ P_y = 6 + \frac{-30}{11} $$
$$ P_z = 8 + \frac{-45}{11}) $$

## Hodina 69.

Kuželosečky  

Středovy tvar rovnice kruznice

$$(x - m)^2 * ( y - n)^2 = r^2$$

## Hodina 70.

$$ n = 0 $$
$$ m = 0 $$

$$ x^2 + y^2 = 29 $$

- - -

$$ S[-2,3] $$
$$ r = 4 $$

$$ (x + 2)^2 + ( y - 3)^2 = 16 $$

- - -

$$ (x - 3)^2 + (y + 5)^2 = 64 $$

$$ r = 8 $$
$$ S[3,-5] $$

- - - 
 
$$ x^2 + 2xm + y^2 - 2yn + p = 0 $$

$$x^2 + y^2 + 4x - 10y + 28 = 0$$
$$ m = -2 $$
$$ n = 5 $$
$$ r^2 = m^2 + n^2 + p = 4 + 25 - 28 = 1 $$
$$x^2 + y^2 - 8x - 8y + 7 = 0 $$
$$ m = -4 $$
$$ n = 4  $$

## Hodina 71.

$$ k \in A[-6,2],B[1,3],C[-7,9]$$

$$ \vec{u}(B-A) = (7,1)  $$
$$ \vec{v}(C-B) = (-8,6) $$

Staci najit dva stredy usecek

$$ S_AB = (x = \frac{A_x + B_x}{2},y = \frac{A_y + B_y}{2})$$
$$ S_AB = (-2.5,2.5)$$
$$ S_VC = (-3,6)$$

Udelam obecne rovnice  

$$ 7x + y + c = 0 $$
$$ 7*(-2.5) + 2.5 + c = 0 $$
$$ -17.5 + 2.5 + 15 = 0 $$
$$c = 15$$

$$ -8x + 6y + c = 0 $$
$$ 24 + 36 + c = 0 $$
$$ c = -60 $$

$$ 7x + y + 15 = 0 | * -6 $$
$$ -8x + 6y -60 = 0 $$

udelam vypocet pod sebou  

$$ -42x - 6y - 90 = 0$$
$$ -8x + 6y -60 = 0 $$

$$ -50x - 150 = 0$$
$$ -50x = 150 $$
$$ x = -3 $$

$$ -21 + y + 15 = 0 $$
$$ y = 6 $$

$$S[-3,6]$$

$$(-6+3)^2 + (2-6)^2 = r^2$$
$$9 + 16 = r^2 $$
$$ r = 5 $$

## Hodina 72.

$$ x^2 + y^2 + 6x - 12y + 20 = 0 $$
$$ p: x + 2y + 6$$
$$ p_x = -2y - 6$$

$$(6-2y)^2 + y^2 - 36 + 12y - 12y + 20 = 0 $$
$$36 + 4y^2 + y^2 - 12y - 12y + 20 = 0 $$

$$5y^2 - 48y + 92 = 0$$


$$D = b^2 - 4ac $$
$$D = 2304 - 1840 $$

$$D > 0 $$
D je vetsi nez 0 takze jsou dve realna reseni a je to secna  
pokud by D bylo 0 tak je to tecna  
pokud by bylo mensi nez 0 nemela by prusecik  

$$y_1 = \frac{-48 + \sqrt{464}}{10}$$
$$y_2 = \frac{-48 - \sqrt{464}}{10}$$

$$x_1 = -2(\frac{-48 + \sqrt{464}}{10})$$
$$x_2 = -2(\frac{-48 - \sqrt{464}}{10})$$

$$x_1 = -7.9$$
$/$x_2 = 1.3$$

- - -


$$ x^2 + y^2 + 6x - 12y + 20 = 0 $$
$$p: -3x - 4y + 10 = 0 $$

$$ -3x = 4y - 10$$ 
$$ x = \frac{-4y}{3} + \frac{10}{3}$$

$$ p_x = \frac{-4y}{3} + \frac{10}{3} $$

$$ (\frac{-4y}{3} + \frac{10}{3})^2 + y^2 + 6(\frac{-4y}{3} + \frac{10}{3}) - 12y + 20 = 0 $$

$$\frac{16y^2}{9} + \frac{80y}{3} + \frac{100}{9} + y^2 - \frac{-24y}{3} - \frac{60}{3} = 0 $$

$$y^2 - 12 y + 36 = 0$$

## Hodina 72.

$$f: x + y = 2 $$
$$f_x = 2 - y$$

$$ k: x^2 + y^2 + 6x - 12y + 20 = 0$$
$$ (2 - y)^2 + y^2 + 6(2 - y) - 12y + 20 = 0$$
$$ 4 - y^2 + y^2 + 12 - 6y - 12y + 20 = 0 $$
KURVA  
$$ y^2 - 11y + 18 = 0 $$
Spatne  
$$y_1 =  9 $$
$$y_2 =  2 $$
$$x_1 = -7 $$
$$x_2 =  0 $$

musim najit takove M a N aby byly podminky  

$$(m - y)^2 + y^2 + 6(m - y) -12y + 20 = 0$$
$$m^2 - my - my + y^2 + y^2 + 6m - 6y - 12y + 20 = 0 $$
$$m^2 -2my + 2y^2 + 6m - 18y + 20 = 0$$
$$2y^2 - 18y - 2my + m^2 + 6m + 20 = 0$$

$$a = 2$$
$$b = -18 - 2m$$
$$c = m^2 + 6m + 20$$

$$D = 324 + 72m + 4m^2 - 8m^2 - 48m - 160 $$
$$D = 164 + 24m - 4m^2$
$$D = -m^2 + 6m + 49 $$$


$$D_1 = 36 + 164$$
$$D = 200$$

$$m_12 = \frac{-6 \plusmn \sqrt{200}}{-2}$$
$$m_12 = \frac{-6 \plusmn 10\sqrt{2}}{-2}$$

$$m_1 = 3 + 5\sqrt{10}$$
$$m_2 = 3 - 5\sqrt{10}$$

$$ k: x^2 + y^2 + 6x - 12y + 20 = 0$$
$$ l: (x-2)^2 + (y - 10)^2 = 2$$


